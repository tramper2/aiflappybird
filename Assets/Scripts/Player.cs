﻿//이곳을 에이전트로 바꾸주고

using UnityEngine;
using UnityEngine.UI;
using Unity.MLAgents;           //사용할 
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using System.Linq;
using static Unity.MLAgents.Sensors.RayPerceptionOutput;

public class Player : Agent
{
    [SerializeField] private Text scoreText;
    [SerializeField] private RayPerceptionSensorComponent2D sensor;

    public Sprite[] sprites;
    public float strength = 0.002f;
    public float gravity = -9.81f;
    public float tilt = 5f;

    private SpriteRenderer spriteRenderer;
    private Vector3 direction;
    private int spriteIndex;

    Rigidbody2D playerRB;

    int score;

    /*  //학습에서는 없앤다
    private void Start()
    {
        InvokeRepeating(nameof(AnimateSprite), 0.15f, 0.15f);  //날개짓 하는 애니메이션인듯
    }*/

    public override void Initialize()  //초기화
    {
        playerRB = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    public override void OnEpisodeBegin()  //이곳이 시작 할때 처리인데...
    {
        score = 0;
        Vector3 position = transform.position;
        position.y = 0f;
        transform.position = position;
        direction = Vector3.zero;
        InvokeRepeating(nameof(AnimateSprite), 0.15f, 0.15f);  //날개짓 하는 애니메이션인듯
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        //어떤 센서값을 사용할 것인지 특히 위치 같은것
        sensor.AddObservation(transform.position.y); //player의 y값을 관측해라 벨로시티를 관측해야 하나?
        sensor.AddObservation(playerRB.velocity.y);

        //센서로 보고 있는거라 사실 필요가 없는 작업으로 보인다
        /*
        Transform pipPos = checkRayCast();
        if(pipPos != null) {
            sensor.AddObservation(pipPos.position.y);
        }*/


    }


    //센서로 보고 있는거라 사실 필요가 없는 작업으로 보인다

    private Transform checkRayCast()
    {
        var rayOutputs = RayPerceptionSensor.Perceive(sensor.GetRayPerceptionInput(), true).RayOutputs;

        int lengthOfRayOutputs = rayOutputs.Length;

        for (int i = 0; i < lengthOfRayOutputs; i++)
        {
            GameObject goHit = rayOutputs[i].HitGameObject;
            if (goHit != null)
            {
                var rayDirection = rayOutputs[i].EndPositionWorld - rayOutputs[i].StartPositionWorld;
                var scaledRayLength = rayDirection.magnitude;
                float rayHitDistance = rayOutputs[i].HitFraction * scaledRayLength;

                return goHit.transform;
                // Print info:
                string dispStr = "";
                dispStr = dispStr + "__RayPerceptionSensor - HitInfo__:\r\n";
                dispStr = dispStr + "GameObject name: " + goHit.name + "\r\n";
                dispStr = dispStr + "Hit distance of Ray: " + rayHitDistance + "\r\n";
                dispStr = dispStr + "GameObject tag: " + goHit.tag + "\r\n";
                Debug.Log(dispStr);
            }
        }
        return null;

    }




    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        //player를 콘트롤 현재는 점프 하나 밖에 없는 상태이므로 
        //var continuousActions = actionBuffers.ContinuousActions;  //행동이 2가지가 있는데... 컨티뉴우스는 쎄기?
        var discreteActions = actionBuffers.DiscreteActions;   //점프키 액션은 이걸 이용하면 되는 듯

        bool JumpCommand = discreteActions[0] > 0; //0보다 크면 트루가 되면 점프를 실행
        if (JumpCommand)
        {
            //direction = Vector3.up * strength;
            //여길 리지드바디로 줘야 할지..
            playerRB.velocity = Vector3.up * strength;
           // playerRB.AddForce(Vector3.up * strength);
        }

    }

    private void Update()
    {
        //에이전트가 눌러서 동작하게 해야 한다
       // if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) {  //키보드도 가능하고 마우스도 가능하다
       //     direction = Vector3.up * strength;
       //  }
       /*
        // Apply gravity and update the position
        direction.y += gravity * Time.deltaTime;
        transform.position += direction * Time.deltaTime;

        // Tilt the bird based on the direction
        Vector3 rotation = transform.eulerAngles;
        rotation.z = direction.y * tilt;
        transform.eulerAngles = rotation;
       */
    }

    //키보드 조종을 위한 향후 참고용 
    //*
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var discreteActions = actionsOut.DiscreteActions;

        if (Input.GetKey(KeyCode.Space))
        {
            discreteActions[0] = 1;
        }
        else
        {
            discreteActions[0] = 0;
        }

        discreteActions[0] = Input.GetKey(KeyCode.Space) ? 1 : 0;
    }
    //*/

    private void AnimateSprite()  //날개짓 하는 애니메이션을 반복한다
    {
        spriteIndex++;

        if (spriteIndex >= sprites.Length) {
            spriteIndex = 0;
        }

        if (spriteIndex < sprites.Length && spriteIndex >= 0) {
            spriteRenderer.sprite = sprites[spriteIndex];
        }
    }


    //파이프와 충돌이 발생했을때 처리
    private void OnTriggerEnter2D(Collider2D other)  //충돌햇을 경우에 처리 곧 죽으면 점수 계산후 다시 시작해야 한다
    {
        if (other.gameObject.CompareTag("Obstacle")) {
            //GameManager.Instance.GameOver(); //게임이 끝나는게 아니고... 학습을 재시작 해야 하는데...
            AddReward(-1f);// 벌점을 지불한다
            
            EndEpisode();
        } else if (other.gameObject.CompareTag("Scoring")) {
            //GameManager.Instance.IncreaseScore();
            AddReward(1f); //보상을 지불한다
            score += 1;
        }
        scoreText.text = score.ToString();
    }

}
